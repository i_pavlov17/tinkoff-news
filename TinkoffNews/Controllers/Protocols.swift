//
//  Protocols.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 03/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import Foundation
import CoreData

typealias ThrowsResultClosure<T> = () throws -> (T)
typealias ThrowsCompletion<T> = (@escaping ThrowsResultClosure<T>) -> Void

/// ManagedObject's extension
protocol Managed: class, NSFetchRequestResult {
    static var entityName: String { get }
    static var defaultSortDescriptors: [NSSortDescriptor] { get }
}

/// Networking protocol
protocol ApiHTTP {

    /// Method to obtain data from server
    ///
    /// - Parameters:
    ///   - endpoint: endpoint to fetch data
    ///   - completion: throwing completion of oepration
    func perform(endpoint: ApiEndpoint, completion: ThrowsCompletion<Data>?)
}

typealias FeedLoadCompletion = ThrowsCompletion<Void>

/// News feed storage
protocol Feed {

    /// Request more posts in addition to loaded ones.
    /// - parameter completion: Throwing completion containing operation result.
    func loadMore(completion: FeedLoadCompletion?)

    /// Resets feed
    ///
    /// - Parameter completion: Throwing completion of operation
    func reset(completion: @escaping ThrowsCompletion<Void>)
}

/// News info storage
protocol NewsStorage {

    /// Request to load content of selected material.
    ///
    /// - Parameters:
    ///   - id: identifier of post
    ///   - completion: throwing completion of operation. Completion is used to deliver errors
    func obtainContent(for id: NewsIdentifier, completion: @escaping ThrowsCompletion<Void>)

    /// Request to increment number of views for particular news
    ///
    /// - Parameter id: news identifier
    func incrementViewsCount(for id: NewsIdentifier, completion: @escaping ThrowsCompletion<Void>)
}

/// Let's suppose, that we don't want to use NSPersistentContainer, because minimal target is 9.0
protocol CoreData {

    /// Main context for reading data for UI
    var viewContext: NSManagedObjectContext { get }

    /// Context for synchronization purposes. Writing to this context will not block main thread
    var syncContext: NSManagedObjectContext { get }

    /// Context for saving data. Writing will block main thread
    var saveContext: NSManagedObjectContext { get }

    /// Force configuration of core data stack
    func configure()
}
