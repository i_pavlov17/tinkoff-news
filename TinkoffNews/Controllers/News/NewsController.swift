//
//  NewsController.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 07/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import Foundation
import CoreData

class NewsController: NewsStorage {

    // MARK: - Dependencies
    let api: ApiHTTP
    let coreData: CoreData

    /// We can use DI, but for current purposes, it will be a singleton
    static let shared: NewsStorage = NewsController(api: ApiHTTPController.shared, coreData: CoreDataController.shared)

    // MARK: - Lifecycle
    init(api: ApiHTTP, coreData: CoreData) {
        self.api = api
        self.coreData = coreData
    }

    // MARK: - NewsStorage

    func obtainContent(for id: NewsIdentifier, completion: @escaping ThrowsCompletion<Void>) {
        let endpoint: ApiEndpoint = .fetchPostContent(id: id)
        self.api.obtainValue(byExecuting: endpoint) { [weak self]  (result: () throws -> ApiResponse<NewsContent>) in
            do {
                let content: ApiResponse<NewsContent> = try result()
                self?.save(newsContent: content.payload, for: id, completion: completion)
            } catch {
                completion { throw error }
            }
        }
    }

    func incrementViewsCount(for id: NewsIdentifier, completion: @escaping ThrowsCompletion<Void>) {
        let context = self.coreData.syncContext
        context.perform {
            do {
                guard let entity: NewsItem = try self.obtainItem(by: id, in: context) else {
                    DispatchQueue.main.async {
                        completion { throw LogicError.unknownError }
                    }
                    return
                }
                // A dirty hack to trigger FRC update
                if let statistics = entity.statistics {
                    entity.statistics = self.incrementViewsCount(statistics: statistics)
                }
                context.saveOrRollback()
                DispatchQueue.main.async {
                    completion {}
                }
            } catch {
                DispatchQueue.main.async {
                    completion { throw error }
                }
            }
        }
    }

    // MARK: - Private methods

    private func incrementViewsCount(statistics: NewsItemStatistics) -> NewsItemStatistics {
        let previousValue = statistics.views
        let newValue = previousValue + 1
        statistics.views = newValue
        print("update views from \(previousValue) to \(newValue)")
        return statistics
    }

    private func obtainItem(by id: NewsIdentifier, in context: NSManagedObjectContext) throws -> NewsItem? {
        let fetchRequest: NSFetchRequest<NewsItem> = NewsItem.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id = \(id)")
        return try context.fetch(fetchRequest).first
    }

    private func save(newsContent: NewsContent, for id: NewsIdentifier, completion: @escaping ThrowsCompletion<Void>) {
        let saveContext = self.coreData.syncContext
        saveContext.perform {
            do {
                guard let entity: NewsItem = try self.obtainItem(by: id, in: saveContext) else {
                    DispatchQueue.main.async {
                        completion { throw LogicError.unknownError }
                    }
                    return
                }
                entity.content = newsContent.content
                saveContext.saveOrRollback()
                DispatchQueue.main.async {
                    completion {}
                }
            } catch {
                DispatchQueue.main.async {
                    completion { throw error }
                }
            }
        }
    }
}
