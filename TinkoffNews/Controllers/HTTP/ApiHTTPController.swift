//
//  ApiHTTPController.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 03/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import Foundation

class ApiHTTPController: ApiHTTP {

    private lazy var session: URLSession = {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.urlCache = nil
        configuration.timeoutIntervalForRequest = 10
        configuration.urlCredentialStorage = nil
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        return URLSession(configuration: configuration)
    }()

    /// We can use DI, but for current purposes, it will be a singleton
    static let shared: ApiHTTP = ApiHTTPController()

    // MARK: - Public methods

    func perform(endpoint: ApiEndpoint, completion: ThrowsCompletion<Data>?) {
        guard let url = endpoint.URL else {
            assertionFailure("URL is incorrect")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = endpoint.method.rawValue
        self.perform(request: request, completion: completion)
    }

    // MARK: - Private methods

    private func perform(request: URLRequest, completion: ThrowsCompletion<Data>?) {
        let task = self.session.dataTask(with: request) { (data, response, error) in
            guard let completionHandler = completion else { return }
            if let error = error {
                if error.isTimeout() || error.isNoInternet() {
                    completionHandler { throw ApiError.noInternetConnection }
                } else {
                    completionHandler { throw error }
                }
                return
            }
            guard let httpResponse = response as? HTTPURLResponse else {
                completionHandler { throw ApiError.unknownError }
                return
            }
            let statusCode = httpResponse.statusCode
            switch (data, statusCode, error) {
            // no network connection
            case let (_, _, error?) where error.isTimeout() || error.isNoInternet():
                completionHandler { throw ApiError.noInternetConnection }
                return
            // success
            case let (data?, httpCode, nil) where 200...299 ~= httpCode:
                completionHandler { data }
                return
            // server errors
            case let (_, httpCode, _) where 500...599 ~= httpCode:
                completionHandler { throw ApiError.internalServerError(statusCode: httpCode) }
                return
            // other errors
            case let (_, httpCode, error):
                completionHandler { throw ApiError.commonError(underlyingError: error, statusCode: httpCode) }
                return
            }
        }
        task.resume()
    }

    // MARK: Lifecycle
    deinit {
        self.session.invalidateAndCancel()
    }
}

extension ApiHTTP {
    func obtainValue<T: Decodable>(byExecuting endpoint: ApiEndpoint, completion: @escaping ThrowsCompletion<T>) {
        self.perform(endpoint: endpoint) { result in
            do {
                let data = try result()
                let jsonDecoder = JSONDecoder()
                let result: T = try jsonDecoder.decode(T.self, from: data)
                completion { return result }
            } catch let error {
                completion { throw error }
            }
        }
    }
}
