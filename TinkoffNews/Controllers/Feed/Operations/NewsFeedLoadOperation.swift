//
//  NewsFeedLoadOperation.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 06/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import Foundation
import CoreData

class NewsFeedLoadOperation: Operation {

    // MARK: - Dependencies
    let api: ApiHTTP
    let coreData: CoreData

    // MARK: - Public vars
    let firstCursor: Int
    let lastCursor: Int

    // MARK: - Private vars
    private let completion: ThrowsCompletion<Void>?
    private let callBackQueue: OperationQueue

    // MARK: - Boilerplate for async operations
    override var isAsynchronous: Bool { return true }
    override var isExecuting: Bool { return state == .executing }
    override var isFinished: Bool { return state == .finished }

    private(set) var state = State.ready {
        willSet {
            self.willChangeValue(forKey: state.keyPath)
            self.willChangeValue(forKey: newValue.keyPath)
        }
        didSet {
            self.didChangeValue(forKey: state.keyPath)
            self.didChangeValue(forKey: oldValue.keyPath)
        }
    }

    enum State: String {
        case ready = "Ready"
        case executing = "Executing"
        case finished = "Finished"
        fileprivate var keyPath: String { return "is" + self.rawValue }
    }

    override func start() {
        if self.isCancelled {
            self.state = .finished
        } else {
            self.state = .ready
            self.main()
        }
    }

    override func main() {
        if self.isCancelled {
            self.state = .finished
        } else {
            self.state = .executing
        }
        self.loadData()
    }

    // MARK: - Lifecycle

    init(api: ApiHTTP, coreData: CoreData, firstCursor: Int, lastCursor: Int, completion: @escaping ThrowsCompletion<Void>) {
        self.api = api
        self.coreData = coreData
        self.firstCursor = firstCursor
        self.lastCursor = lastCursor
        self.completion = completion
        self.callBackQueue = OperationQueue.current ?? OperationQueue.main
        super.init()
    }

    // MARK: - Private methods

    private func loadData() {
        print(self.firstCursor)
        let endpoint: ApiEndpoint = .fetchPosts(first: self.firstCursor, last: self.lastCursor)
        self.api.obtainValue(byExecuting: endpoint) { [weak self]  (result: () throws -> ApiResponse<[News]>) in
            do {
                let response = try result()
                self?.save(newsList: response.payload) {
                    self?.completion? {}
                    self?.state = .finished
                }
            } catch {
                self?.completion? { throw error }
                self?.state = .finished
            }
        }
    }

    // MARK: - Private methods
    private func save(newsList: [News], completion: @escaping () -> Void) {
        let syncContext = self.coreData.syncContext
        syncContext.perform {
            for news in newsList {
                if let fetchedItem = self.obtainItem(by: news.id, in: syncContext) {
                    self.updateItem(item: fetchedItem, with: news)
                } else {
                   self.cache(news: news, in: syncContext)
                }
            }
            syncContext.saveOrRollback()
            completion()
        }
    }

    private func updateItem(item: NewsItem, with fetchedNews: News) {
        item.name = fetchedNews.name
        item.publicationDate = fetchedNews.publicationDate
        item.text = fetchedNews.text
    }

    private func cache(news: News, in syncContext: NSManagedObjectContext) {
        let id = Int32(news.id)
        let newsItem = NewsItem.insert(into: syncContext, id: id, name: news.name, text: news.text, publicationDate: news.publicationDate)
        let statistics = self.obtainStatistics(for: news.id, in: syncContext)
        newsItem.statistics = statistics
        statistics?.newsItem = newsItem
    }

    private func obtainStatistics(for id: Int, in context: NSManagedObjectContext) -> NewsItemStatistics? {
        let request: NSFetchRequest<NewsItemStatistics> = NewsItemStatistics.fetchRequest()
        request.predicate = NSPredicate(format: "itemID == \(id)")
        request.fetchLimit = 1
        do {
            if let statistics = try context.fetch(request).first {
                return statistics
            }
            return NewsItemStatistics.insert(into: context, views: 0, itemID: id)
        } catch {
            context.rollback()
            return nil
        }
    }

    private func obtainItem(by id: NewsIdentifier, in context: NSManagedObjectContext) -> NewsItem? {
        let request: NSFetchRequest<NewsItem> = NewsItem.fetchRequest()
        request.predicate = NSPredicate(format: "id == \(id)")
        request.fetchLimit = 1
        do {
            return try context.fetch(request).first
        } catch {
            context.rollback()
            return nil
        }
    }
}
