//
//  FeedController.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 04/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import Foundation
import CoreData

class FeedController: Feed {

    // MARK: - Constants
    private let pageSize = 20

    // MARK: - Dependencies
    private let api: ApiHTTP
    private let coreData: CoreData

    // MARK: - Public vars

    /// We can use DI, but for current purposes, it will be a singleton
    static let shared: Feed = FeedController(api: ApiHTTPController.shared, coreData: CoreDataController.shared)

    // MARK: - Private vars
    private var lastKnownCursor: Int {
        let fetchRequest: NSFetchRequest<NewsItem> = NewsItem.fetchRequest()
        return (try? coreData.viewContext.count(for: fetchRequest)) ?? 0
    }

    private let newsFetchQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        return queue
    }()

    /// Queue is used for synchronization purposes
    private let loadMoreQueue: DispatchQueue = DispatchQueue(label: "loadMoreQueue")

    // MARK: - Lifecycle

    init(api: ApiHTTP, coreData: CoreData) {
        self.api = api
        self.coreData = coreData
    }

    // MARK: - Feed protocol

    func loadMore(completion: FeedLoadCompletion?) {
        self.loadMoreQueue.async {
            let firstCursor = self.lastKnownCursor
            let lastCursor = firstCursor + self.pageSize

            guard !self.isOperationAlreadyExists(firstCursor: firstCursor) else {
                completion? {}
                return
            }
            let operation = NewsFeedLoadOperation(api: self.api, coreData: self.coreData, firstCursor: firstCursor, lastCursor: lastCursor) { result in
                do {
                    _ = try result()
                    completion? {}
                } catch {
                    completion? { throw error }
                }
            }
            self.newsFetchQueue.addOperation(operation)
        }
    }

    func reset(completion: @escaping ThrowsCompletion<Void>) {
        self.loadMoreQueue.sync {
            self.removeAllItems()
            DispatchQueue.main.async {
                completion {}
            }
        }
    }

    // MARK: - Private methods

    private func removeAllItems() {
        let context = self.coreData.syncContext
        context.performAndWait {
            let request: NSFetchRequest<NewsItem> = NewsItem.fetchRequest()
            if let newsItems = try? context.fetch(request) {
                for item in newsItems {
                    context.delete(item)
                }
            }
            try? context.save()
        }
    }

    private func isOperationAlreadyExists(firstCursor: Int) -> Bool {
        return self.newsFetchQueue.operations.contains(where: {  operation -> Bool in
            guard let newsOperation = operation as? NewsFeedLoadOperation else { return false }
            return newsOperation.firstCursor == firstCursor
        })
    }
}
