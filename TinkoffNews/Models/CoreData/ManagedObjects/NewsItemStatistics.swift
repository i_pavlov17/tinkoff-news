//
//  NewsItemStatistics.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 07/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import Foundation
import CoreData

extension NewsItemStatistics: Managed {
    static var defaultSortDescriptors: [NSSortDescriptor] {
        return [NSSortDescriptor(key: #keyPath(itemID), ascending: false)]
    }
}

extension NewsItemStatistics {
    @discardableResult static func insert(into context: NSManagedObjectContext, views: Int, itemID: Int) -> NewsItemStatistics {
        let newsItemStatistics: NewsItemStatistics = context.insertObject()
        newsItemStatistics.views = Int32(views)
        newsItemStatistics.itemID = Int32(itemID)
        return newsItemStatistics
    }
}
