//
//  NewsItem.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 05/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import Foundation
import CoreData

extension NewsItem: Managed {
    static var defaultSortDescriptors: [NSSortDescriptor] {
        return [NSSortDescriptor(key: #keyPath(id), ascending: false)]
    }
}

extension NewsItem {
    @discardableResult static func insert(into context: NSManagedObjectContext, id: Int32, name: String, text: String, publicationDate: Date) -> NewsItem {
        let newsItem: NewsItem = context.insertObject()
        newsItem.id = id
        newsItem.name = name
        newsItem.text = text
        newsItem.publicationDate = publicationDate
        return newsItem
    }
}
