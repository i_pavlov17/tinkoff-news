//
//  ApiEndpoint.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 03/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import Foundation

typealias Parameters = [String: Any]

enum ApiEndpoint {
    case fetchPosts(first: Int?, last: Int?)
    case fetchPostContent(id: Int)
}

// MARK: public vars

extension ApiEndpoint {

    var URLString: String {
        switch self {
        case .fetchPosts:
            return "news"
        case .fetchPostContent:
            return "news_content"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .fetchPosts, .fetchPostContent:
            return .get
        }
    }

    var parameters: Parameters? {
        switch self {
        case let .fetchPosts(first, last):
            var parameters = Parameters()
            parameters["first"] = first
            parameters["last"] = last
            return parameters.isEmpty ? nil : parameters
        case .fetchPostContent(let id):
            return ["id": id]
        }
    }
}

extension ApiEndpoint {
    var URL: URL? {
        let urlString = baseAPIURL + self.URLString
        guard var components = URLComponents(string: urlString) else {
            return nil
        }
        if self.method == .get {
            components.queryItems = self.parameters?.compactMap {
                guard let stringConvertibleValue = $0.value as? CustomStringConvertible else { return nil }
                return URLQueryItem(name: $0.key, value: "\(stringConvertibleValue)")
            }

        }
        return components.url
    }
}
