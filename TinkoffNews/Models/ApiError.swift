//
//  ApiError.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 03/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import Foundation

enum ApiError: Error {
    case commonError(underlyingError: Error?, statusCode: Int?)
    case noInternetConnection
    case internalServerError(statusCode: Int)
    case incorrectServerResponse
    case unknownError
}
