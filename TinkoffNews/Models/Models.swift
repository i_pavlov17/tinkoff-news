//
//  Models.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 04/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import Foundation

typealias NewsIdentifier = Int
typealias BankInfoType = Int

struct ApiResponse<PayloadType: Decodable>: Decodable {
    let payload: PayloadType
    let resultCode: String

    enum CodingKeys: String, CodingKey {
        case payload
        case resultCode
    }
}

struct News {
    let id: NewsIdentifier
    let name: String
    let text: String
    let publicationDate: Date
    let bankInfoTypeId: BankInfoType

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case text
        case bankInfoTypeId
        case publicationDate
        case milliseconds
    }
}

struct NewsContent: Decodable {
    let content: String
}
