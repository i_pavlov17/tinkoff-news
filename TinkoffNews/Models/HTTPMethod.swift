//
//  HTTPMethod.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 03/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case get
    case post
    case put
    case delete
}
