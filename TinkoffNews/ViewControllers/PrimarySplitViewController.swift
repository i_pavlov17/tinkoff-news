//
//  PrimarySplitViewController.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 07/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import UIKit

class PrimarySplitViewController: UISplitViewController, UISplitViewControllerDelegate {

    override func viewDidLoad() {
        self.delegate = self
        self.preferredDisplayMode = .allVisible
    }

    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController,
                             onto primaryViewController: UIViewController) -> Bool {
        return true
    }
}
