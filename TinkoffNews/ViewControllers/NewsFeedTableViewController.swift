//
//  NewsFeedTableViewController.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 04/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import UIKit
import CoreData

class NewsFeedTableViewController: UITableViewController {

    // MARK: - Dependencies
    var feed: Feed = FeedController.shared
    var coreData: CoreData = CoreDataController.shared

    // MARK: - Private vars
    private var dataSource: TableViewDataSource<NewsFeedTableViewController>!

    lazy var pullToRefreshControl: UIRefreshControl = {
        let pullToRefreshControl = UIRefreshControl()

        pullToRefreshControl.addTarget(self, action: #selector(NewsFeedTableViewController.pullToRefreshDragged(sender:)), for: UIControlEvents.valueChanged)

        return pullToRefreshControl
    }()

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshControl = self.pullToRefreshControl
        self.setupTableView()
        self.pagination()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        if let destination = segue.destination as? NewsContentViewController, let newsCell = sender as? NewsTableViewCell {
            destination.newsItem = newsCell.newsItem
        }
    }

    // MARK: - Private methods
    private func setupTableView() {
        let request = NewsItem.sortedFetchRequest
        request.fetchBatchSize = 20
        request.returnsObjectsAsFaults = false

        let fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: self.coreData.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        self.dataSource = TableViewDataSource(tableView: tableView, cellIdentifier: NewsTableViewCell.className, fetchedResultsController: fetchedResultsController, delegate: self)
    }

    private func pagination() {
        self.feed.loadMore { [weak self] result in
            do {
                _ = try result()
                self?.hideRefreshControl()
            } catch {
                self?.show(error: error)
            }
        }
    }

    // MARK: - PullToRefresh

    @objc func pullToRefreshDragged(sender: UIRefreshControl) {
        self.feed.reset { [weak self] result in
            do {
                _ = try result()
                self?.pagination()
            } catch {
                DispatchQueue.main.async {
                    self?.show(error: error)
                }
            }
        }
    }

    private func hideRefreshControl() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.pullToRefreshControl.endRefreshing()
        }
    }
}

// MARK: - TableViewDataSourceDelegate
extension NewsFeedTableViewController: TableViewDataSourceDelegate {
    func tableViewDataSource<Delegate>(_ dataSource: TableViewDataSource<Delegate>,
                                       didShowSpinnerCell spinnerCell: SpinnerTableViewCell) where Delegate: TableViewDataSourceDelegate {
        self.pagination()
    }

    func tableViewDataSourceDidEndUpdate<Delegate>(_ dataSource: TableViewDataSource<Delegate>) where Delegate: TableViewDataSourceDelegate {
        self.hideRefreshControl()
    }

    func configure(_ cell: NewsTableViewCell, for object: NewsItem) {
        cell.newsItem = object
    }
}
