//
//  NewsTableViewCell.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 04/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    private static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .none
        return formatter
    }()

    // MARK: - Outlets
    @IBOutlet private weak var newsTextLabel: UILabel!
    @IBOutlet private weak var viewsCountLabel: UILabel!
    @IBOutlet private weak var newsDateLabel: UILabel!

    // MARK: - Public vars
    var newsItem: NewsItem? {
        didSet {
            guard let newsItem = newsItem, let text = newsItem.text, let statistics = newsItem.statistics, let date = newsItem.publicationDate else { return }
            self.configure(newsText: text, viewsCount: Int(statistics.views), date: date)
        }
    }

    // MARK: - Private methods
    private func configure(newsText: String, viewsCount: Int, date: Date) {
        self.newsTextLabel.text = newsText
        self.viewsCountLabel.text = "\(viewsCount)"
        self.newsDateLabel.text = NewsTableViewCell.dateFormatter.string(from: date)
    }
}
