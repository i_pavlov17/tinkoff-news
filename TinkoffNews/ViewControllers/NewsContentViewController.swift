//
//  NewsContentViewController.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 07/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import UIKit

class NewsContentViewController: UIViewController {

    private static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        return formatter
    }()

    private let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [.documentType: NSAttributedString.DocumentType.html,
                                                                               .characterEncoding: String.Encoding.utf8.rawValue]

    // MARK: - Dependencies
    let newsStorage: NewsStorage = NewsController.shared

    // MARK: - Outlets
    @IBOutlet weak var textView: UITextView!

    // MARK: - Public vars
    var newsItem: NewsItem? {
        didSet {
            guard let newsItem = newsItem else {
                self.newsItemObserver = nil
                return
            }
            self.startObserve(newsItem: newsItem)
            self.incrementViewsCount(newsItem: newsItem)
            self.updateTitle(newsItem: newsItem)
            self.fetch(newsItem: newsItem)
            self.refreshContent()
        }
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshContent()
    }

    // MARK: - Private vars
    private var newsItemObserver: ManagedObjectObserver?

    // MARK: - Private methods

    private func incrementViewsCount(newsItem: NewsItem) {
        newsItem.managedObjectContext?.perform {
            self.newsStorage.incrementViewsCount(for: Int(newsItem.id)) { [weak self] result in
                do {
                    _ = try result()
                } catch {
                    self?.show(error: error)
                }
            }
        }
    }

    private func updateTitle(newsItem: NewsItem) {
        newsItem.managedObjectContext?.perform {
            guard let date = newsItem.publicationDate else { return }
            DispatchQueue.main.async {
                self.title = NewsContentViewController.dateFormatter.string(from: date)
            }
        }
    }

    private func startObserve(newsItem: NewsItem) {
        self.newsItemObserver = ManagedObjectObserver(object: newsItem) { [weak self] change in
            switch change {
            case .update:
                self?.refreshContent()
            case .delete:
                break
            }
        }
    }

    private func fetch(newsItem: NewsItem) {
        let identifier = Int(newsItem.id)
        self.newsStorage.obtainContent(for: identifier) { [weak self] result in
            do {
                _ = try result()
            } catch {
                self?.show(error: error)
            }
        }
    }

    private func refreshContent() {
        self.newsItem?.managedObjectContext?.perform {
            guard let htmlString = self.newsItem?.content else { return }
            DispatchQueue.main.async {
                self.textView.attributedText = self.html(from: htmlString)
            }
        }
    }

    private func html(from string: String) -> NSAttributedString? {
        let data = Data(string.utf8)
        do {
            return try NSAttributedString(data: data, options: self.options, documentAttributes: nil)
        } catch {
            self.show(error: error)
            return nil
        }
    }
}
