//
//  TableViewDataSource.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 05/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import Foundation
import CoreData
import UIKit

protocol TableViewDataSourceDelegate: class {
    associatedtype Object: NSFetchRequestResult
    associatedtype Cell: UITableViewCell
    func configure(_ cell: Cell, for object: Object)

    func tableViewDataSource<Delegate: TableViewDataSourceDelegate>(_ dataSource: TableViewDataSource<Delegate>, didShowSpinnerCell spinnerCell: SpinnerTableViewCell)
    func tableViewDataSourceDidEndUpdate<Delegate: TableViewDataSourceDelegate>(_ dataSource: TableViewDataSource<Delegate>)
}

enum Sections: Int {
    case content = 0
    case spinner = 1

    init?(section: Int) {
        self.init(rawValue: section)
    }
}

/// Note: this class doesn't support working with multiple sections
class TableViewDataSource<Delegate: TableViewDataSourceDelegate>: NSObject, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate {
    typealias Object = Delegate.Object
    typealias Cell = Delegate.Cell

    // MARK: - Lifecycle
    required init(tableView: UITableView, cellIdentifier: String, fetchedResultsController: NSFetchedResultsController<Object>, delegate: Delegate) {
        self.tableView = tableView
        self.cellIdentifier = cellIdentifier
        self.fetchedResultsController = fetchedResultsController
        self.delegate = delegate
        super.init()
        self.fetchedResultsController.delegate = self
        try? fetchedResultsController.performFetch()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.reloadData()
    }

    // MARK: - Public vars and methods

    var selectedObject: Object? {
        guard let indexPath = self.tableView.indexPathForSelectedRow else { return nil }
        return objectAtIndexPath(indexPath)
    }

    func objectAtIndexPath(_ indexPath: IndexPath) -> Object {
        return self.fetchedResultsController.object(at: indexPath)
    }

    func reconfigureFetchRequest(_ configure: (NSFetchRequest<Object>) -> Void) {
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: fetchedResultsController.cacheName)
        configure(self.fetchedResultsController.fetchRequest)
        do { try self.fetchedResultsController.performFetch() } catch { fatalError("fetch request failed") }
        self.tableView.reloadData()
    }

    // MARK: Private
    fileprivate let tableView: UITableView
    fileprivate let fetchedResultsController: NSFetchedResultsController<Object>
    fileprivate weak var delegate: Delegate!
    fileprivate let cellIdentifier: String

    // MARK: UITableViewDataSource & Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        guard let section = Sections(rawValue: section) else { return 0 }
        switch section {
        case .content:
            guard let section = self.fetchedResultsController.sections?[section.rawValue] else { return 0 }
            return section.numberOfObjects
        case .spinner:
            return 1
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = Sections(rawValue: indexPath.section) else {
            fatalError("Unexpected section type at \(indexPath)")
        }
        switch section {
        case .content:
            let object = self.fetchedResultsController.object(at: indexPath)
            let cell: Cell = tableView.dequeueCell(for: indexPath)
            self.delegate.configure(cell, for: object)
            return cell
        case .spinner:
            let cell: SpinnerTableViewCell = tableView.dequeueCell(for: indexPath)
            self.delegate.tableViewDataSource(self, didShowSpinnerCell: cell)
            return cell
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? SpinnerTableViewCell {
            cell.startAnimating()
        }
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? SpinnerTableViewCell {
            cell.stopAnimating()
        }
    }

    // MARK: NSFetchedResultsControllerDelegate
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any,
                    at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            guard let indexPath = newIndexPath else { fatalError("Index path should be not nil") }
            self.tableView.insertRows(at: [indexPath], with: .fade)
        case .update:
            guard let indexPath = indexPath else { fatalError("Index path should be not nil") }
            let object = objectAtIndexPath(indexPath)
            guard let cell = self.tableView.cellForRow(at: indexPath) as? Cell else { break }
            self.delegate.configure(cell, for: object)
        case .move:
            guard let indexPath = indexPath else { fatalError("Index path should be not nil") }
            guard let newIndexPath = newIndexPath else { fatalError("New index path should be not nil") }
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            self.tableView.insertRows(at: [newIndexPath], with: .fade)
        case .delete:
            guard let indexPath = indexPath else { fatalError("Index path should be not nil") }
            self.tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
        self.delegate.tableViewDataSourceDidEndUpdate(self)
    }
}
