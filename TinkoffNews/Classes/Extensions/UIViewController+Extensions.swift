//
//  UIViewController+Extensions.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 07/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import UIKit

extension UIViewController {
    func show(error: Error) {
        DispatchQueue.main.async {
            var errorMessage: String? = error.localizedDescription
            if let error = error as? ApiError {
                errorMessage = error.errorDescription
            }
            let unwrappedMessage = errorMessage ?? "Что-то пошло не так"
            self.present(UIAlertController(title: nil, dismissableMessage: unwrappedMessage), animated: true, completion: nil)
        }
    }
}
