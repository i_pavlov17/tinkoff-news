//
//  NSObject+Extensions.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 05/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import Foundation

public extension NSObject {
    // MARK: - Public vars
    public class var className: String {
        return String(describing: self)
    }

    public var className: String {
        return String(describing: type(of: self))
    }
}
