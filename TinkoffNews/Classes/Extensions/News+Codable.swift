//
//  News+Codable.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 06/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import Foundation

extension News: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .name)
        self.text = try container.decode(String.self, forKey: .text)
        self.bankInfoTypeId = try container.decode(Int.self, forKey: .bankInfoTypeId)

        let stringIdentifier = try container.decode(String.self, forKey: .id)
        guard let identifier = Int(stringIdentifier) else {
            let context = DecodingError.Context(codingPath: decoder.codingPath,
                                                debugDescription: "Could not parse string key to a Int")
            throw DecodingError.dataCorrupted(context)
        }
        self.id = identifier

        let publicationDateContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .publicationDate)

        let timeInterval = try publicationDateContainer.decode(TimeInterval.self, forKey: .milliseconds)
        self.publicationDate = Date(timeIntervalSince1970: timeInterval / 1000)
    }
}
