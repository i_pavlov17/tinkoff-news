//
//  Utilities.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 07/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import Foundation

extension Sequence where Element: AnyObject {
    func containsObjectIdentical(to object: AnyObject) -> Bool {
        return contains { $0 === object }
    }
}

extension Array {
    var decomposed: (Iterator.Element, [Iterator.Element])? {
        guard let item = first else { return nil }
        return (item, Array(self[1..<count]))
    }
}
