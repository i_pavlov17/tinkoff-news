//
//  UIAlertController+Extensions.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 07/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import UIKit

extension UIAlertController {
    convenience init(title: String?, dismissableMessage: String?) {
        self.init(title: title, message: dismissableMessage, preferredStyle: .alert)
        self.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
    }
}
