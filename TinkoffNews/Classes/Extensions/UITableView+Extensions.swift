//
//  UITableView+Extensions.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 07/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import UIKit

extension UITableView {
    func dequeueCell<T>(for indexPath: IndexPath) -> T where T: UITableViewCell {
        guard let cell = self.dequeueReusableCell(withIdentifier: T.className, for: indexPath) as? T else {
            preconditionFailure("Unable to dequeue \(T.description()) for indexPath: \(indexPath)")
        }
        return cell
    }
}
