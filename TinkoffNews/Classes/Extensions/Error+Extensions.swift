//
//  Error+Extensions.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 03/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import Foundation

extension Error {
    func isNoInternet() -> Bool {
        return [NSURLErrorCannotFindHost, NSURLErrorCannotConnectToHost, NSURLErrorNetworkConnectionLost,
                NSURLErrorDNSLookupFailed, NSURLErrorHTTPTooManyRedirects, NSURLErrorResourceUnavailable,
                NSURLErrorNotConnectedToInternet, NSURLErrorInternationalRoamingOff, NSURLErrorDataNotAllowed,
                NSURLErrorSecureConnectionFailed, NSURLErrorCannotLoadFromNetwork]
            .contains((self as NSError).code)
    }

    func isTimeout() -> Bool {
        return (self as NSError).code == NSURLErrorTimedOut
    }

    func isRequestCancelledError() -> Bool {
        return (self as NSError).code == NSURLErrorCancelled
    }
}
