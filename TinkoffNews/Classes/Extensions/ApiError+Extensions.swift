//
//  ApiError+Extensions.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 07/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import Foundation

extension ApiError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .noInternetConnection: return "Отсутствует сетевое соединение"

        default: return nil
        }
    }
}
