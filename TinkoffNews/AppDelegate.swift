//
//  AppDelegate.swift
//  TinkoffNews
//
//  Created by Ivan Pavlov on 03/09/2018.
//  Copyright © 2018 Ivan Pavlov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let coreData: CoreData = CoreDataController.shared

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.coreData.configure()
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        self.saveContext()
    }

    // MARK: - private methods

    /// Context saving is a complex operation, so we will save it before app terminating
    private func saveContext() {
        let context = self.coreData.saveContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                fatalError("Unresolved error \(error)")
            }
        }
    }
}
